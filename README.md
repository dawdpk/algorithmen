*Algorithmen*

Dieses Repository dient der Sammlung der Eigenimplementierungen aus der Vorlesung zu Algorithmen.

Sie erheben keinen Anspruch auf Korrektheit, Performanz oder Eleganz.