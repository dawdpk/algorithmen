import java.util.ArrayList;

public class GGT1 {
	
	public static void main(String[] args){
		
		// initialize
		int sieveSize = 132;
		int numberOne = 84;
		int numberTwo = 231;
		
		// Creation of sieve of eratosthenes
		int[] s = eratos(sieveSize);
		
		// Collect prime factors for first number
		ArrayList<Integer> f1 = checkForPrimeFactors(s, numberOne);
		// Collect prime factors for second number
		ArrayList<Integer> f2 = checkForPrimeFactors(s, numberTwo);
		// Check for common factors and create ggt
		int ggt = commonFactorsAndGGT(f1, f2);
		System.out.println(ggt);
	}
	
	private static int commonFactorsAndGGT(ArrayList<Integer> f1,
			ArrayList<Integer> f2) {
		
		int ggt = 1;
		for (Integer factor : f1) {
			if(f2.contains(factor)){ ggt *= factor;}
		}
		
		return ggt;
	}

	private static ArrayList<Integer> checkForPrimeFactors(int[] s, int n) {
		
		ArrayList<Integer> f = new ArrayList<Integer>();
		
		for (int i = 2; i < s.length; i++) {
			if (s[i] == 1 && n > 1){
				while (n%i == 0) {
					f.add(Integer.valueOf(i));
					n = n/i; 
				}
			}
		}
		
		return f;
	}

	private static int[] eratos(int n){
		int[] s = new int[n];
		s[0] = 0; s[1] = 0;
		for (int i = 2; i < n; i++){
			s[i] = 1;
		}
		
		for (int i = 0; i*i<n; i++) {
			if(s[i] == 1){
				for (int k = 2*i; k < n; k += i){
					s[k] = 0;
				}
			}
		}
		
		return s;
	}
}
