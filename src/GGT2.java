public class GGT2 {
	
	public static void main(String[] args){
		
		// initialize
		int numberOne = 84;
		int numberTwo = 231;
		
		// calculate ggt with euklid algorithm
		int ggt = euklid(numberOne, numberTwo);
		System.out.println(ggt);
	}

	private static int euklid(int numberOne, int numberTwo) {
		int temp;
		
		while (numberOne > 0){
			if (numberOne < numberTwo){
				temp = numberOne;
				numberOne = numberTwo;
				numberTwo = temp;
			}
			numberOne = numberOne % numberTwo;
		}

		return numberTwo;
	}
}
