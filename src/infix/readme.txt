Ablaufbeschreibung InfixInterpreter
===================================

Vorbereitungen
--------------
Zu Beginn der Kalkulation in der Methode calc() wird der �bergebene Term um eventuelle Leerzeichen
bereinigt und eine Validit�ts�berpr�fung in der Methode isValidTerm() findet statt.
Anschlie�end findet die Initialisierung der beiden ben�tigten Stacks f�r die Operatoren und
Operanden statt. Des Weiteren wird Term mit Klammern umgeben, die im sp�teren Verlauf f�r eine
Evaluierung verantwortlich ist.
Abschlie�end wird der Term durch einen Tokenizer in einzelne Teile zerlegt, getrennt nach
Operatoren (+-*/()) und Operanden.

Hauptschleife
-------------
Solange weitere Elemente (Tokens) im Tokenizer vorliegen, wird f�r jedes Element der
Schleifenk�rper durchlaufen. In diesem finden Fallunterscheidungen statt:

a)	Es handelt sich um eine �ffnende Klammer:
	Die Klammer wird auf den Operatorstack gelegt.
	
b)	Es handelt sich um einen Operanden:
	Hierbei wird zuerst gepr�ft, ob ein Negierer im Operanden enthalten, damit dieser ber�cksich-
	tigt werden kann. Anschlie�end der wird Wert auf den Operandenstack gelegt.
	
c)	Es handelt sich um eine schlie�ende Klammer:
	Es wird eine innere Schleife durchlaufen, die abgebrochen wird, bis das n�chste Element auf dem
	Operatorstack eine �ffnende Klammer ist. Dies bedeutet, dass alle notwendigen Kalkulationen
	durchgef�hrt wurden.
	Innerhalb der Schleife wird ein Operator vom Operatorstack genommen und zwei Operanden vom
	Operandenstack. Diese werden nun entsprechend ihrer Kommutativit�t berechnet und das Ergebnis
	auf den Operandenstack gelegt.
	Abschlie�end (nach Durchlauf der Schleife) wird die �ffnende Klammer vom Operatorstack entfernt, da diese mit der ausl�senden,
	schlie�enden Klammer korrespondierte und nun aufgel�st werden kann.

d)	Es handelt sich um einen Operator +-*/:
	Es wird eine innere Schleife durchlaufen, die abgebrochen wird, wenn der Operatorstock leer ist
	oder die Pr�zedenz des oben auf dem Operatorstack liegenden Operators geringer ist als die
	Pr�zedenz des aktuellen Operators.
	Innerhalb der Schleife wird ein Operator vom Operatorstack genommen und zwei Operanden vom
	Operandenstack. Diese werden nun entsprechend ihrer Kommutativit�t berechnet und das Ergebnis
	auf den Operandenstack gelegt.
	Abschlie�end (nach Durchlauf der Schleife) wird der aktuelle Operator auf den Stack gelegt.

Ausgabe
-------
Zum Abschluss wird das letzte verbliebene Element auf dem Operandenstack ausgegeben. Dieses
entspricht dem Ergebnis der Berechnung des Terms.