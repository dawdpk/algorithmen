package liste;

public class Liste {
	
	static Node head;
	static Node tail;
	
	public void init(){
		head = new Node();
		tail = new Node();
		
		head.next = tail;
		tail.next = tail;
	}
	
	public void deleteFollowingNode(Node n){
		Node temp = n.next;
		n.next = n.next.next;
		if (temp != tail) temp = null;
	}
	
	public Node addAfterNode(int value, Node n){
		Node newNode = new Node();
		newNode.key = value;
		newNode.next = n.next;
		n.next = newNode;
		return newNode;
	}
	
	public Node search(int v) {
		tail.key = v;
		Node searchnode = head;
		while (searchnode.key != v){
			searchnode = searchnode.next;
		}
		return searchnode;
	}

	public void modify(int srcV, int destV){
		Node nodeToChange = search(srcV);
		if (nodeToChange.key != tail.key){
			nodeToChange.key = destV;
		} else {
			System.out.println("Node not found");
		}
	}
	
	public void showAll(){
		Node stepNode = head;
		do{
			System.out.println(stepNode.toString() + ": "+stepNode.key);
			stepNode = stepNode.next;
		}while(stepNode != tail);
	}
	
	public Node getHead() {
		return head;
	}

	public void setHead(Node head) {
		Liste.head = head;
	}

	public Node getTail() {
		return tail;
	}

	public void setTail(Node tail) {
		Liste.tail = tail;
	}
}
