package liste;

public class ListenRunner {
	
	public static void main(String[] args){
		Liste list = new Liste();
		list.init();
		list.addAfterNode(23, list.getHead());
		list.addAfterNode(42, list.getHead());
		list.showAll();
		list.modify(23, 89);
		list.showAll();
	}
}
