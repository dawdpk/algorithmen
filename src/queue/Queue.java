package queue;

import java.util.ArrayList;

public class Queue {
	private ArrayList<String> queue;
	
	public Queue() {
		this.queue = new ArrayList<String>();
	}

	public void put (String value) {
		queue.add(value);
	}
	
	public String get () {
		String value = queue.get(0);
		queue.remove(0);
		return value;
	}
	
	
	public ArrayList<String> getQueue() {
		return queue;
	}

	public void setQueue(ArrayList<String> queue) {
		this.queue = queue;
	}
}
