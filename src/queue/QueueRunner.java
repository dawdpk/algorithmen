package queue;


public class QueueRunner {

	public static void main(String[] args) {
		Queue queue = new Queue();
		
		queue.put("hallo");
		queue.put("welt");
		
		for(String s : queue.getQueue()){
			System.out.println(s);
		}
		
		System.out.println("===================");
		
		System.out.println(queue.get());
		System.out.println(queue.get());

		System.out.println("===================");
		for(String s : queue.getQueue()){
			System.out.println(s);
		}
	}
}
