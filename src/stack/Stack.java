package stack;

import java.util.ArrayList;

public class Stack {
	private ArrayList<String> stack;
	
	public Stack() {
		this.stack = new ArrayList<String>();
	}

	public void push (String value) {
		stack.add(value);
	}
	
	public String pop () {
		String value = stack.get(stack.size()-1);
		stack.remove(stack.size()-1);
		return value;
	}
	
	
	public ArrayList<String> getStack() {
		return stack;
	}

	public void setStack(ArrayList<String> stack) {
		this.stack = stack;
	}
}
