package stack;

public class StackRunner {

	public static void main(String[] args) {
		Stack stack = new Stack();
		
		stack.push("hallo");
		stack.push("welt");
		
		for(String s : stack.getStack()){
			System.out.println(s);
		}
		
		System.out.println("===================");
		
		System.out.println(stack.pop());
		System.out.println(stack.pop());

		System.out.println("===================");
		for(String s : stack.getStack()){
			System.out.println(s);
		}
	}
}
